// Example project which returns a function exposing a list of fruit
module.exports = {
  getInventory() {
    return [
      { label: 'apples', stock: 2 },
      { label: 'bananas', stock: 9 },
      { label: 'grapes', stock: 0 },
      { label: 'kiwis', stock: 12 },
      { label: 'lemons', stock: 22 },
      { label: 'oranges', stock: 5 },
      { label: 'pears', stock: 4 },
      { label: 'pineapples', stock: 15 },
      { label: 'strawberries', stock: 6 }
    ];
  }
};
