const fs = require('fs');
const Mustache = require('mustache');
const myApp = require('../src/index.js');
const inventory = myApp.getInventory();

// Get the Mustache template
const homeTemplate = fs.readFileSync(__dirname + '/templates/home.mst', 'utf-8');

// Get the fruit with the highest quantity
const topProduct = inventory.reduce((acc, curr) => {
  if (acc === null || curr.stock > acc.stock) {
    return curr;
  } else {
    return acc;
  }
}, null);

// Render the page using your variables
const homeContent = Mustache.render(homeTemplate, { inventory, topProduct });

// Write the file in a build directory
const buildDir = __dirname + '/../build';
if (!fs.existsSync(buildDir)) {
  fs.mkdirSync(buildDir);
}

fs.writeFileSync(buildDir + '/home.md', homeContent);
