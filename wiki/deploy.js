const fs = require('fs');
const Axios = require('axios');
const qs = require('qs');

const config = {
  gitlabBaseUrl: 'https://gitlab.com', // Update this if you're on a private Gitlab
  projectId: process.env.CI_PROJECT_ID, // Provided by Gitlab-CI
  privateToken: process.env.WIKI_DEPLOY_TOKEN, // Added through Gitlab interface
  buildDir: __dirname + '/../build'
};

const axios = Axios.create({
  baseURL: config.gitlabBaseUrl,
  headers: { 'Private-Token': config.privateToken, Accept: 'application/json' }
});

(async function deploy() {
  const existingPages = await getExistingWikiPages();
  const updatedPages = getUpdatedPages();
  // Pages which existed but are no longer in the build (obsolete)
  const pagesToDelete = existingPages.filter(p1 => updatedPages.every(p2 => p2.slug !== p1.slug));
  // Pages which didn't exist before
  const pagesToCreate = updatedPages.filter(p1 => existingPages.every(p2 => p2.slug !== p1.slug));
  // Pages which already exist
  const pagesToUpdate = updatedPages.filter(p1 => existingPages.some(p2 => p2.slug === p1.slug));

  console.log(
    `Found ${pagesToDelete.length} pages to delete, ${pagesToCreate.length} pages to create, ${pagesToUpdate.length} pages to update.`
  );
  for (let page of pagesToDelete) {
    await deletePage(page);
  }
  for (let page of pagesToCreate) {
    await createPage(page);
  }
  for (let page of pagesToUpdate) {
    await updatePage(page);
  }
  console.log('Deploy complete!');
})();

function getExistingWikiPages() {
  return axios.get(`/api/v4/projects/${config.projectId}/wikis`).then(res => res.data);
}

function getUpdatedPages() {
  const files = fs.readdirSync(config.buildDir);
  return files.map(file => {
    const name = file // Remove the file extension
      .split('.')
      .slice(0, -1)
      .join('.');
    return {
      format: 'markdown', // You could make this depend on the file extension
      slug: name,
      title: name,
      content: fs.readFileSync(`${config.buildDir}/${file}`, 'utf-8')
    };
  });
}

function deletePage(page) {
  console.log(`Deleting ${page.slug}...`);
  return axios.delete(`/api/v4/projects/${config.projectId}/wikis/${page.slug}`);
}

function createPage(page) {
  console.log(`Creating ${page.slug}...`);
  return axios.post(`/api/v4/projects/${config.projectId}/wikis`, qs.stringify(page));
}

function updatePage(page) {
  console.log(`Updating ${page.slug}...`);
  return axios.put(`/api/v4/projects/${config.projectId}/wikis/${page.slug}`, qs.stringify(page));
}
